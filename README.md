## Installation

```bash
$ npm install
$ npm install -g ts-node
```

## Running the app

```bash
# run app service
$ npm start

# run sync service
$ npm run start-sync

# run full synchronization
$ npm run start-sync-re-index

# run both of dervice
$ npm run start-both

# run prettier
$ npm run pretty

```
