import { User } from './user.entity'
import crypto from 'crypto'
import { Collection, MongoClient } from 'mongodb'
import * as dotenv from 'dotenv'
dotenv.config()

const argv = process.argv.slice(2)

if (!process.env.DB_URI) {
    console.error('DB_URI variable not defined in .env')
    process.exit(1)
}
const client = new MongoClient(process.env.DB_URI)

const BATCH_SIZE = 1000

enum Collections {
    CUSTOMERS = 'customers',
    CUSTOMERS_ANONYMIZED = 'customers_anonymised',
}

async function main() {
    console.log('Sync service runnig!')
    await client.connect()
    const db = client.db(process.env.DB_NAME)
    const sourceCollection = db.collection(Collections.CUSTOMERS)
    const targetCollection = db.collection(Collections.CUSTOMERS_ANONYMIZED)

    if (argv.includes('--full-reindex')) {
        console.log('Starting full reindex...')
        await syncCollections(sourceCollection, targetCollection)
        console.log('Full reindex completed successfully.')
        process.exit(0)
    }

    const lastDocument = await sourceCollection.findOne(
        {},
        { sort: { _id: -1 } }
    )
    const lastDocumentId = lastDocument?._id || 0

    const lastAnonymisedDocument = await targetCollection.findOne(
        {},
        { sort: { _id: -1 } }
    )
    const lastAnonymisedDocumentId = lastAnonymisedDocument?._id || 0

    if (lastDocumentId !== lastAnonymisedDocumentId) {
        console.log('Start sync!')
        // If the data was not synchronized while the service was offline, perform synchronization.
        await syncCollections(sourceCollection, targetCollection)
        console.log('Sync completed!')
    }

    const changeStream = sourceCollection.watch()
    const documents: User[] = []
    let timer: NodeJS.Timeout | null = null

    changeStream.on('change', function (change: any) {
        const document: User = change.fullDocument
        const anonymousDocument = anonymiseDocument(document)
        documents.push(anonymousDocument)

        if (documents.length === BATCH_SIZE || timer === null) {
            timer = setTimeout(() => {
                documents.splice(1000)
                targetCollection
                    .insertMany(documents)
                    .then(() => {
                        console.log(
                            `Inserted ${documents.length} documents into customers_anonymised`
                        )
                        documents.length = 0 // очищаем массив
                    })
                    .catch((err) => {
                        console.error(
                            `Error inserting documents into customers_anonymised: ${err}`
                        )
                    })
                timer = null // сбрасываем таймер
            }, 1000)
        }
    })
}

function anonymiseString(seed: string) {
    // Generating a random number based on seed
    // The createHash function is used to obtain a deterministic hash from the seed
    const hash = crypto.createHash('sha256').update(seed).digest('hex')
    const random = parseInt(hash.slice(0, 8), 16)

    let result = ''
    const characters =
        'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    for (let i = 0; i < 8; i++) {
        const index = (random >> (i * 5)) & 0x1f
        result += characters.charAt(index)
    }
    return result
}

function anonymiseDocument(document: User) {
    const [email, domain] = document.email.split('@')
    return {
        ...document,
        firstName: anonymiseString(document.firstName),
        lastName: anonymiseString(document.lastName),
        email: `${anonymiseString(email)}@${domain}`,
        address: {
            ...document.address,
            line1: anonymiseString(document.address.line1),
            line2: anonymiseString(document.address.line2),
            postcode: anonymiseString(document.address.postcode),
        },
    }
}

async function syncCollections(
    sourceCollection: Collection,
    targetCollection: Collection
) {
    // delete all records from the target collection
    await targetCollection.deleteMany({})

    // Get all records from the source collection.
    const allDocuments = await sourceCollection.find({}).toArray()

    // anonymize them and insert them into the target collection.
    const anonymousDocuments = allDocuments.map((document: any) =>
        anonymiseDocument(document)
    )

    await targetCollection.insertMany(anonymousDocuments)
}

main()
