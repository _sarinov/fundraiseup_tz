import { faker } from '@faker-js/faker'
import { User } from './user.entity'
import { MongoClient } from 'mongodb'
import * as dotenv from 'dotenv'
dotenv.config()

if (!process.env.DB_URI) {
    console.error('DB_URI variable not defined in .env')
    process.exit(1)
}
const client = new MongoClient(process.env.DB_URI)

async function main() {
    console.log('App service runnig!')
    try {
        // Connect to the MongoDB server
        await client.connect()
        console.log('Connected to MongoDB server')

        // Get a reference to the "customers" collection
        const customersCollection = client
            .db(process.env.DB_NAME)
            .collection('customers')

        // Continuously generate customers and insert them into the "customers" collection
        while (true) {
            // Generate a random number of customers (between 1 and 10)
            const chunk = Math.floor(Math.random() * 10) + 1

            // Generate the customers
            const customers: User[] = []
            for (let i = 0; i < chunk; i++) {
                customers.push(createRandomUser())
            }

            // Insert the customers into the "customers" collection
            const result = await customersCollection.insertMany(customers)
            console.log(`Inserted ${result.insertedCount} customers`)

            // Wait for 200 milliseconds before generating the next batch of customers
            await new Promise((resolve) => setTimeout(resolve, 200))
        }
    } catch (err) {
        console.error(err)
    } finally {
        await client.close()
        console.log('Disconnected from MongoDB server')
    }
}

function createRandomUser(): User {
    return {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        address: {
            line1: faker.address.streetAddress(),
            line2: faker.address.secondaryAddress(),
            postcode: faker.address.zipCode(),
            city: faker.address.city(),
            state: faker.address.state(),
            country: faker.address.country(),
        },
    }
}

main()
